package com.example.thandroidkt;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LogInActivity extends AppCompatActivity {

    EditText edtusername;
    EditText edtpassword;
    Button btnLogin, btnRegister;
    CheckBox cbRemember;
    FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        anhxa();

        auth = FirebaseAuth.getInstance();
        btnRegister.setOnClickListener(view->{
            startActivity(new Intent(LogInActivity.this, SignUpActivity.class));
        });
        btnLogin.setOnClickListener(View->{
            loginUser();
        });
    }
    public void anhxa(){
        edtusername = findViewById(R.id.editTextUSERNAME);
        edtpassword = findViewById(R.id.editTextTextPASSWORD);
        btnLogin = findViewById(R.id.buttonLOGIN);
        cbRemember = findViewById(R.id.checkBoxREMEMBERME);
        btnRegister = findViewById(R.id.buttonREGISTER);
    }

    private void loginUser(){
        String email = edtusername.getText().toString();
        String pass = edtpassword.getText().toString();
        if (email.isEmpty() || pass.isEmpty()){
            Toast.makeText(this, "User name or Passwork cannot be empty", Toast.LENGTH_SHORT).show();
            edtusername.requestFocus();
        }else {
            auth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(LogInActivity.this, "Login Successfull", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(LogInActivity.this, ListActivity.class));
                    }else {
                        Toast.makeText(LogInActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}