package com.example.thandroidkt;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignUpActivity extends AppCompatActivity {

    EditText etUserName;
    EditText etPassWord;
    Button btnSignUp;
    TextView tvLoginHere;
    FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        etPassWord = findViewById(R.id.etPassWord);
        etUserName = findViewById(R.id.etUserName);
        btnSignUp = findViewById(R.id.btnSignUp);
        tvLoginHere = findViewById(R.id.tvLoginHere);

        auth = FirebaseAuth.getInstance();

        tvLoginHere.setOnClickListener(view ->{
            startActivity(new Intent(SignUpActivity.this, LogInActivity.class));
        });
        btnSignUp.setOnClickListener(view->{
            createUser();
        });
    }

    private void createUser(){
        String email = etUserName.getText().toString();
        String pass = etPassWord.getText().toString();
        if (email.isEmpty() || pass.isEmpty()){
            Toast.makeText(this, "User name or Passwork cannot be empty", Toast.LENGTH_SHORT).show();
            etUserName.requestFocus();
        }else {
            auth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()){
                        Toast.makeText(SignUpActivity.this, "User registered successfully", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(SignUpActivity.this, LogInActivity.class));
                    }else{
                        Toast.makeText(SignUpActivity.this, "Registeration error", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}