package com.example.thandroidkt;

public class Tree {
    //tên khoa học, tên thường gọi, đặc tính, màu lá, công dụng, dược tính, chú ý, hình ảnh
    int idtree;
    int imageid;
    String sciencename;
    String commonname;
    String specie;
    String leavecolor;
    String uses;
    String medicinal;
    String note;

    public Tree(int idtree, int imageid, String sciencename, String commonname, String specie, String leavecolor, String uses, String medicinal, String note) {
        this.idtree = idtree;
        this.imageid = imageid;
        this.sciencename = sciencename;
        this.commonname = commonname;
        this.specie = specie;
        this.leavecolor = leavecolor;
        this.uses = uses;
        this.medicinal = medicinal;
        this.note = note;
    }

    public int getIdtree() {
        return idtree;
    }

    public void setIdtree(int idtree) {
        this.idtree = idtree;
    }

    public int getImageid() {
        return imageid;
    }

    public void setImageid(int imageid) {
        this.imageid = imageid;
    }

    public String getSciencename() {
        return sciencename;
    }

    public void setSciencename(String sciencename) {
        this.sciencename = sciencename;
    }

    public String getCommonname() {
        return commonname;
    }

    public void setCommonname(String commonname) {
        this.commonname = commonname;
    }

    public String getSpecie() {
        return specie;
    }

    public void setSpecie(String specie) {
        this.specie = specie;
    }

    public String getLeavecolor() {
        return leavecolor;
    }

    public void setLeavecolor(String leavecolor) {
        this.leavecolor = leavecolor;
    }

    public String getUses() {
        return uses;
    }

    public void setUses(String uses) {
        this.uses = uses;
    }

    public String getMedicinal() {
        return medicinal;
    }

    public void setMedicinal(String medicinal) {
        this.medicinal = medicinal;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
